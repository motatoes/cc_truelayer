const {AuthAPIClient, DataAPIClient} = require("truelayer-client");
const app = require("express")();

const redirect_uri = "http://localhost:5000/truelayer-redirect";

// Create TrueLayer client instance
const client = new AuthAPIClient({
    client_id: "choicetransfer-6zrc",
    client_secret: "m9jk1go9vlf19eh38k03ae"
});

// Define array of permission scopes
const scopes = ["info", "accounts", "balance", "transactions", "offline_access"]

// Construct url and redirect to the auth dialog
app.get("/", (req, res) => {
    const authURL = client.getAuthUrl(redirect_uri, scopes, "foobar", null, true);
    res.redirect(authURL);
});

// Retrieve 'code' query-string param, exchange it for access token and hit data api
app.get("/truelayer-redirect", async (req, res) => {
    console.log(req.query)
    const code = req.query.code;
    const tokens = await client.exchangeCodeForToken(redirect_uri, code);
    console.log('b')
    const info = await DataAPIClient.getInfo(tokens.access_token);

    res.set("Content-Type", "text/plain");
    res.send(`Access Token: ${JSON.stringify(info, null, 2)}`);
});

app.listen(5000, () => console.log("Example app listening on port 5000..."));