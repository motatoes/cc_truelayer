const {AuthAPIClient, DataAPIClient} = require("truelayer-client");
const app = require("express")();
const Parser = require("body-parser");
const fs = require("fs")
const redirect_uri = "http://localhost:5000/truelayer-redirect";

// Create TrueLayer client instance
const client = new AuthAPIClient({
    // client_id: "bananas-6fn5",
    // client_secret: "e94kollgfljg8rc3xp5or9"
    client_id: "choicetransfer-6zrc",
    client_secret: "m9jk1go9vlf19eh38k03ae"    
});

// Define array of permission scopes
const scopes = ["info", "accounts", "balance", "transactions", "offline_access"]

// Construct url and redirect to the auth dialog
app.get("/", (req, res) => {
    const authURL = client.getAuthUrl(redirect_uri, scopes, "foobar", "form_post", "bar", true);
    res.redirect(authURL);
});

// Body parser setup
app.use(Parser.urlencoded({ extended: true }));

// Retrieve 'code' query-string param, exchange it for access token and hit data api
app.post("/truelayer-redirect", async (req, res) => {

    const code = req.body.code;
    const tokens = await client.exchangeCodeForToken(redirect_uri, code);
    const info = await DataAPIClient.getInfo(tokens.access_token);


    fs.writeFile("accesstoken.txt", tokens.access_token, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    }); 

    fs.writeFile("refreshtoken.txt", tokens.refresh_token, function(err) {
        if(err) {
            return console.log(err);
        }
        console.log("The refresh file was saved!");
    }); 

    //var accounts = await DataAPIClient.getAccounts(tokens.access_token);
    return res.redirect("http://localhost:8001/")
    // res.set("Content-Type", "text/plain");
    // res.send(`Access Token: ${JSON.stringify(info, null, 2)}`);
    // res.send(accounts)
    // console.log(accounts)
    
});

app.get("/accounts", (req, res) => {

    fs.readFile("accesstoken.txt", 'utf8', async (err, accesstoken) => {
        if (err) throw err;
        console.log(accesstoken);

        // var dclient = new DataAPIClient()
        var accounts = await DataAPIClient.getAccounts(accesstoken);
        res.set("Content-Type", "text/json");
        res.send(accounts);
    })
});

app.get("/refresh", (req, res) => {

    fs.readFile("refreshtoken.txt", 'utf8', async (err, refreshtoken) => {
        if (err) throw err;
        // console.log("old token: " + accesstoken);
        var newtokenobj = await client.refreshAccessToken(refreshtoken)
        console.log(newtokenobj.access_token)

        // overwrite the new token
        fs.writeFile("accesstoken.txt", newtokenobj.access_token, (err) => {
            if(err) {
                return console.log(err)
            }
            console.log("The new access token was saved!")
        });

        // overwrite the refresh token
        fs.writeFile("refreshtoken.txt", newtokenobj.refresh_token, (err) => {
            if(err) {
                return console.log(err)
            }
            console.log("The new refresh token was saved!")
        });

        res.set("Content-Type", "text/json")
        res.send(newtokenobj.access_token)
    });

});

app.get("/transactions", (req, res) => {

    console.log(req.query.account_id)
    var account_id = req.query.account_id ? req.query.account_id  : "1c6e4dab4429598ff0c6d39ce45c0c24"
    
    var from = req.query.from || "2017-07-29" 
    var to = req.query.to || "2017-09-29"

    console.log(account_id)
    console.log(from)
    console.log(to)

    fs.readFile("accesstoken.txt", 'utf8', async (err, accesstoken) => {
        if (err) throw err;
        console.log(accesstoken);

        // var dclient = new DataAPIClient()
        var transactions = await DataAPIClient.getTransactions(accesstoken, account_id, from, to);
        // console.log(transactions)
        res.set("Content-Type", "text/json");
        res.send(transactions);
    })

});


app.listen(5000, () => console.log("Example app listening on port 5000..."));
